
-- SUMMARY --

* The module help you to work with the Epoch Payment Gateway.

* Epoch is an Internet Payment Service Provider (IPSP) which enables merchants to accept payments online. If you received a bill to your bank or credit card that led you to this page, we may have processed a charge to your account on behalf of an Internet merchant. You can retrieve information about charges to your account by using the forms below or by contacting Epoch world class customer service department 24 hours per day.

* Official Epoch Website - https://epoch.com/en/index.html

For a full description visit the project page:
  
Bug reports, feature suggestions and latest developments:



-- REQUIREMENTS --

* No Dependencies
* Drupal 6

-- INSTALLATION --

* Install as usual.

* Enable the module in administer >> Modules.

-- CONFIGURATION --

* Configure user permissions in administer >> User management >> Access control.

-- USAGE --


-- CUSTOMIZATION --

  
-- CONTACT --

Current maintainers:
* Heshan Wanigasooriya - (heshan@heidisoft.com)
Contact: http://drupal.org/user/199102/contact.

This project has been sponsored by:
* HEIDI Software - http://heidisoft.com